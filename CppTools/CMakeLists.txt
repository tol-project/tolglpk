# configure TOL

# look for tol
find_package(TOL REQUIRED)
if(TOL_FOUND)
  message( STATUS "TOL_INCLUDE_DIR = ${TOL_INCLUDE_DIR}" )
  message( STATUS "TOL_LIBRARIES = ${TOL_LIBRARIES}" )
else(TOL_FOUND)
endif(TOL_FOUND)

#find_package(GSL REQUIRED)
#if(GSL_FOUND)
#  message("GSL_LIBRARIES = ${GSL_LIBRARIES}")
#  message("GSL_LIBRARY = ${GSL_LIBRARY}")
#  message("GSL_INCLUDE_DIR = ${GSL_INCLUDE_DIR}")
#endif(GSL_FOUND)

include_directories(${TOL_INCLUDE_DIR})

add_tol_package(TolGlpk "${CMAKE_CURRENT_SOURCE_DIR}/source/TolGlpk.cpp")

#target_link_libraries(TolGsl PRIVATE ${GSL_LIBRARIES})
